<?php

/**
 * @file
 *
 * Luottokunta Payment method eInvoicing integration for Ubercart. 
 */

 /**
 * Implementation of hook_recurring_info().
 */
 
function uc_luottokunta_recurring_info() {
  $items['luottokunta'] = array(
    'name' => t('Luottokunta'),
    'payment method' => 'luottokunta',
    'module' => 'uc_luottokunta_xml',
    'fee handler' => 'luottokunta_xml',
  );
  return $items;
}