<?php

/**
 *@file
 *
 * Luottokunta Redirect gateway
 *
 */
 
 define('UC_LUOTTOKUNTA_HTML_POST_SERVER', 'https://dmp2.luottokunta.fi/dmp/html_payments');
 
 /**
 * Implementation of hook_payment_gateway()
 */
 
function uc_luottokunta_redirect_payment_gateway() {
  
  $gateways[] = array(
      'id' => 'luottokunta_redirect',
      'title' => 'Luottokunta Redirect',
      'description' => t('Process credit card payments redirecting to a secure Luottokunta server for card details and authorization.'),
      'settings' => 'uc_luottokunta_redirect_settings_form',
      'luottokunta' => 'uc_luottokunta_redirect_charge',
  );

  return $gateways;
}

/**
 * Callback for payment gateway settings.
 */
function uc_luottokunta_redirect_settings_form() {
  $form['uc_luottokunta_redirect_settings']['uc_luottokunta_redirect_txn_type'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction Type'),
    '#description' => t('Transaction type to use with this payment method.'),
    '#options' => array('0' => t('Authorize only'),'1' =>t('Authorize and capture immediately')),
    '#default_value' => variable_get('uc_luottokunta_redirect_txn_type', 0),
  );  
  
  $form['uc_luottokunta_redirect_settings']['uc_luottokunta_redirect_testingmode_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Testing mode'),
    '#description' => t('<strong>IMPORTANT!</strong> Check this if you want to enable testing mode for the Luottokunta DMP -service. Luottokunta requires all the tests to be made with orders of only 1&euro;. Testing mode modifies the order total to 1&euro; no matter what you buy. The user will not see this, modification will be done just before the payment is sent.'),
    '#default_value' => variable_get('uc_luottokunta_redirect_testingmode_enabled', 0),
  );
      
  $form['uc_luottokunta_redirect_settings']['uc_luottokunta_redirect_no_send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Return success always'),
    '#description' => t('Check this if you do not want to redirect to Luottokunta-server but always return a successful payment. Good for testing the gateway code without needing to input Credit Card data.'),
    '#default_value' => variable_get('uc_luottokunta_redirect_no_send', 0),
  );

  return $form;
}

/**
 * Implementation of hook_form_alter().
 * 
 */
function uc_luottokunta_redirect_form_uc_cart_checkout_review_form_alter(&$form, &$form_state) {
  if (($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'luottokunta') {
      unset($form['submit']);
      $form['#prefix'] = '<table style="display: inline; padding-top: 1em;"><tr><td>';
      $form['#suffix'] = '</td><td>'. drupal_get_form('uc_luottokunta_redirect_form', $order) .'</td></tr></table>';
    }
  }
}


function uc_luottokunta_redirect_form($form_state, $order) {

  $amount = $order->order_total;
  
  // If we're on testing mode, modify the amount to 1e
  if(variable_get('uc_luottokunta_redirect_testingmode_enabled', 0)) $amount = 1*100;
  
  // get fields
  $data = _uc_luottokunta_get_postFields($order, $amount);
  $_SESSION['order_id'] = $data['Order_ID'];

  // set form to send back to the completed-page, if we want to test the gateway code only
  if(variable_get('uc_luottokunta_redirect_no_send', 0)){
    $action =  url('cart/luottokunta/completed', array('absolute'=>TRUE));
    $action .= '?LKMAC='._uc_luottokunta_calculate_mac($order->order_id, $reverse = TRUE, $amount); 
  }else $action = UC_LUOTTOKUNTA_HTML_POST_SERVER;
  
  $form['#action'] = $action;

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to payment'),
  );

  return $form;
}



/**
 * function uc_luottokunta_redirect_charge()
 *
 * Provides Redirect gateway usage and preprocessing during payment.
 */
 
function uc_luottokunta_redirect_charge($order_id, $amount) {
  // Load the order.
  $order = uc_order_load($order_id);
  
   
   // If we're on testing mode, reset the amount as 1e
  if(variable_get('uc_luottokunta_redirect_testingmode_enabled', 0)) $amount = "1";
  
  // get fields
  $submit_data = _uc_luottokunta_get_postFields($order, $amount);

  // Translate the data array into a string we can POST.
  $post_fields = array();
  
  foreach ($submit_data as $key => $value) {
    $post_fields[] = $key .'='. urlencode($value);
  }
  
  $curl_data = array();
  $curl_data['data'] = implode('&', $post_fields);
  
  // Charge the card and receive the server reply
  $result = uc_luottokunta_charge($order, $amount, UC_LUOTTOKUNTA_HTML_POST_SERVER, $curl_data);

  // Get the servers response  
  $params = &$_REQUEST;
 
  //make sure this is not hijacked and we have a return MAC in the return
  if(!isset($params['LKMAC'])){
    $error = t('No MAC in success URL!');
    return array('success'=>FALSE, 'comment' => $error, 'message'=>$error, 'uid'=>$order->uid);
  }

  // check for errors
  if($error = _uc_luottokunta_check_errors($params['LKPRC'], $params['LKSRC'])){

    // Log any errors to the watchdog.
    watchdog('uc_luottokunta_redirect', 'Luottokunta Redirect Response error: @error', array('@error' => $error), WATCHDOG_ERROR);

    return array('success'=>FALSE, 'comment' => $error, 'message'=>$error, 'uid'=>$order->uid);

  }else{
    
    $message = t('Order total of @amount was successfully debited from your Credit Card.', array('@amount'=>$amount));
    
    uc_order_comment_save($order_id, $order->uid, $message, 'admin');
    
    //Log the payment
    $result = array(
      'success' => TRUE,
      'comment' => t('Luottokunta payment successful.'),
      'message' => $message,
      'uid' => $order->uid,
    );

    return $result;

  }
}


function _uc_luottokunta_get_postFields($order, $amount){

  $amount *= 100; // amount must be sent in cents
  
  $merchant_id = variable_get('uc_luottokunta_merchant_id', '');
  
  $txn_type = variable_get('uc_luottokunta_redirect_txn_type', 0);

  if(variable_get('uc_luottokunta_redirect_testingmode_enabled', 0)) $amount = 1*100;
  // Calculate mac
  $mac = _uc_luottokunta_calculate_mac($order->order_id, FALSE, $amount, $merchant_id, $txn_type);
  
  
  if($order->order_uid == 0 || !isset($order->order_uid)){
    global $user;
    $uid = $user->uid;
  }else 
    $uid = $order->order_uid;
  
  // Build a description of the order
  $description = array();
  foreach ((array) $order->products as $product) {
    $description[] = $product->qty .'x '. $product->model;
  }
  
  // Set billing info to the description, so we have more than just the order ID to track
  // if there's problems
  $description[] = t('BILLING INFORMATION');
  $description[] = $order->billing_company;
  $description[] = $order->billing_first_name.' '.$order->billing_last_name;
  $description[] = $order->billing_street1;
  $description[] = $order->billing_postal_code.' '.$order->billing_city;
  $description[] = $order->billing_country;

    // Build the POST data for the transaction.
  $submit_data = array(
    'Merchant_Number' => $merchant_id,
    'Card_Details_Transmit' => 0,
    'Language' => 'FI',
    'Device_Category' => 1,
    'Order_ID' => $order->order_id,
    'Customer_ID' => $uid,
    // 'Dossier_ID' => '', // Only for travel agencies and Airlines. Not supported
    'Amount' => $amount,
    'Currency_Code' => 978,
    'Order_Description' => implode(chr(10), $description),
    'Success_Url' =>  url('cart/luottokunta/completed',array('absolute'=>TRUE)),
    'Failure_Url' => url('cart/luottokunta/failure',array('absolute'=>TRUE)),
    'Cancel_Url' => url('cart/luottokunta/cancel',array('absolute'=>TRUE)),
    'Transaction_Type' => $txn_type,
    'Authentication_Mac' => $mac,
  );
  
  return $submit_data;
}