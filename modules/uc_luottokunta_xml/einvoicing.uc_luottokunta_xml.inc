<?php

/**
 * @file
 *
 * Luottokunta XML-gateway eInvoicing integration for Ubercart. 
 */
 
/**
 * Implementation of hook_recurring_access().
 */
 
function uc_luottokunta_xml_recurring_access($fee, $op, $account) {
  if ($fee->fee_handler == 'luottokunta_xml') {
    switch ($op) {
    
      case 'reactivate':
        if ($fee->status == UC_RECURRING_FEE_STATUS_ACTIVE) {
          return UC_RECURRING_ACCESS_DENY;
        }
        else {
          return UC_RECURRING_ACCESS_ALLOW;
        }
        break;

      case 'update':
        if ($fee->status == UC_RECURRING_FEE_STATUS_ACTIVE) {
          return UC_RECURRING_ACCESS_ALLOW;
        }
        else {
          return UC_RECURRING_ACCESS_DENY;
        }
        break;

      case 'suspend':
        if ($fee->status != UC_RECURRING_FEE_STATUS_ACTIVE) {
          return UC_RECURRING_ACCESS_DENY;
        }
      break;
 
      case 'edit':
        if ($fee->status != UC_RECURRING_FEE_STATUS_ACTIVE) {
          return UC_RECURRING_ACCESS_DENY;
        }
      break;

    }
  }

  if ($op == 'cancel' ) {
    if ($fee->status == UC_RECURRING_FEE_STATUS_CANCELLED) {
      return UC_RECURRING_ACCESS_DENY;
    }
  }
}
 

/**
 * Implementation of hook_recurring_info().
 *
 * If eInvoicing is enabled, then use this as a recurring payment
 */
 
function uc_luottokunta_xml_recurring_info() {

    $items['luottokunta_xml'] = array(
      'name' => t('eInvoicing'),
      'payment method' => 'luottokunta',
      'module' => 'uc_recurring',
      'fee handler' => 'luottokunta_xml',
      'renew callback' => 'uc_luottokunta_xml_renew',
      'process callback' => 'uc_luottokunta_xml_process',
      'cancel callback' => 'uc_luottokunta_xml_cancel',
      'saved profile' => TRUE,
      'menu' => array(
        'charge' => 'uc_luottokunta_xml_charge',
        'edit'   => array(
          'title' => t('Update your recurring fee settings'),
          'page arguments' => array('uc_luottokunta_xml_update_form'),
        ),
        'suspend' => array(
          'title' => t('Temporarily suspend the recurring fee'),
          'page arguments' => array('uc_luottokunta_xml_user_suspend_form'),
       ),
        'cancel' => UC_RECURRING_MENU_DEFAULT,
        'reactivate' => array(
          'title' => t('Reactivate recurring fee'),
          'page arguments' => array('uc_luottokunta_xml_user_reactivate_form'),
        ),

      ),
    );
  return $items;
}

/**
 * Process a renewal using the Luottokunta XML-gateway.
 *
 * @param $order
 *   The order object.
 * @param $fee
 *   The fee object.
 * @return
 *   TRUE if renewal succeeded
 */

function uc_luottokunta_xml_renew($order, &$fee) {

  // only active fee's are allowed
  if($fee->status != UC_RECURRING_FEE_STATUS_ACTIVE)
    return FALSE;
    
  $amount = $fee->fee_amount;

  $result = uc_luottokunta_xml_charge($order->order_id, $amount);

  // Handle the result.
  if ($result['success'] == TRUE) {

      watchdog('uc_luottokunta_xml', 'Recurring fee for user @user processed successfully.', array('@user' => $fee->uid));
      uc_order_update_status($order->order_id, 'completed');
      uc_order_comment_save($order->order_id, 0, t('Luottokunta eInvoicing: Recurring fee renewed.'));

  }else{

    uc_order_update_status($order->order_id, 'canceled');
    uc_order_comment_save($order->order_id, 0, t('Luottokunta eInvoicing: payment returned errors. Payment canceled.'));
    watchdog('uc_luottokunta_xml', 'eInvoicing: There was a problem in processing the recurring fee for user @user.', array('@user' => $fee->uid));

  }

  return $result['success'];

}

/**
 * Process the Recurring fee
 * 
 * I need to think this function through. Not sure what to make of it currently
 */
 
function uc_luottokunta_xml_process($order, &$fee) {

  $fee->fee_handler = 'luottokunta_xml';

  // We need to save the rfid, we don't have it yet.
  if (!$fee->rfid) {
    $fee->rfid = uc_recurring_fee_user_save($fee);
  }

  return TRUE;
}

/**
 * Update the recurring fee
 *
 * This probably should be in a separate module
 */

function uc_luottokunta_xml_update_form($form_state, $rfid, $fee_handler){

  // Load fee.
  $fee = uc_recurring_fee_user_load($rfid);

  $form['rfid'] = array(
    '#type' => 'value',
    '#value' => $fee->rfid,
  );
  $form['fee_amount'] = array(
    '#id' => 'fee_amount',
    '#type' => 'textfield',
    '#title' => t('Update your recurring fee amount'),
    '#default_value' => $fee->fee_amount,
    '#weight' => 99,

  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#suffix' => l(t('Cancel'), 'user/'. $fee->uid.'/recurring-fees'),
    '#weight' => 100,
    '#redirect' => 'user/'. $form_state['values']['uid'],
  );
  
  return $form;
  
}

/**
 * Update form processing
 */
 
function uc_luottokunta_xml_update_form_submit(&$form, &$form_state) {
  
  global $user;
  
  $fee_amount = $form_state['values']['fee_amount'];
  $rfid = $form_state['values']['rfid'];

  $fee = uc_recurring_fee_user_load($rfid);
  $old_fee_amount = $fee->fee_amount;
  $fee->fee_amount = $fee_amount;

  if(uc_recurring_fee_user_save($fee)){
    drupal_set_message(t('Your recurring fee has been updated to @amount &euro;.', array('@amount'=>$fee_amount)),'status');

    uc_order_comment_save($fee->order_id, $user->uid, t('<a href="@user-link">@user</a> updated recurring fee amount <a href="@fee-link">@fee</a>. The fee amount was previously @old_amount and is now set to @new_amount.', array('@user-link' => url('user/'. $user->uid), '@user' => $user->name, '@fee-link' => url('admin/store/orders/recurring/view/fee/'. $rfid), '@fee' => $rfid, '@old_amount' => $old_fee_amount, '@new_amount' => $fee->fee_amount)));
  }
}


/**
 * Displays the confirmation form for cancelling a recurring fee.
 */
 
function uc_luottokunta_xml_user_suspend_form($form_state, $rfid, $fee_handler) {
  $suspend_path = uc_referer_uri();
  $fee = uc_recurring_fee_user_load($rfid);

  if ($fee->fee_handler == $fee_handler) {
    $form['uid'] = array('#type' => 'value', '#value' => $fee->uid);
    $form['rfid'] = array('#type' => 'value', '#value' => $rfid);
    
    if($fee->status == UC_RECURRING_FEE_STATUS_SUSPENDED){
      return confirm_form($form, t('Are you sure you want to reactivate this recurring fee?'), $suspend_path, t('This will reactivate your recurring payments back to normal'), t('Yes, reactivate the subscription'), t('No, keep it suspended'));
    }else{
      return confirm_form($form, t('Are you sure you want to suspend this recurring fee?'), $suspend_path, t('You will be reminded monthly about reactivating your subscription'), t('Yes, suspend the subscription'), t('No, I want to continue with the subscription'));
    }
  }
  else {
    drupal_set_message(t('Invalid fee handler was given, try again from the operations for this <a href="@url">recurring fee</a>, if problem persists contact the site administrator.', array('@url' => url($suspend_path))), 'error');
  }
}

/**
 * Implements hook_submit() for the cancel recurring fee form
 */
function uc_luottokunta_xml_user_suspend_form_submit($form, &$form_state) {

  uc_recurring_fee_suspend($form_state['values']['rfid']);

  $fee = uc_recurring_fee_user_load($form_state['values']['rfid']);
  $form_state['redirect'] = 'user/'. $fee->uid;
}


/**
 * Wrapper function to suspend a user's recurring fee.
 *
 * Cancellation is done by setting the status to 3 (UC_RECURRING_FEE_STATUS_SUSPENDED).
 *
 * @param $rfid
 *   The recurring fee's ID.
 * @param $fee
 *   Optional; The loaded fee object.
 */
function uc_recurring_fee_suspend($rfid, $fee = NULL) {
  global $user;
  if (empty($fee)) {
    $fee = uc_recurring_fee_user_load($rfid);
  }
  if($fee->status == UC_RECURRING_FEE_STATUS_SUSPENDED){
    $fee->status = UC_RECURRING_FEE_STATUS_ACTIVE;
    // Add a timestamp to the user cancellation.
    $fee->data['reactivate'] = time();
    $action = 'reactivated';
  }else{
    $fee->status = UC_RECURRING_FEE_STATUS_SUSPENDED;
    // Add a timestamp to the user suspension.
    $fee->data['suspend'] = time();
    $action = 'suspended';
  }


  if(uc_recurring_fee_user_save($fee)){
    
    drupal_set_message(t('The recurring fee has been @action by your request.', array('@action' => $action)),'status');
    
    // Add comment about cancellation in the product.
    uc_order_comment_save($fee->order_id, $user->uid, t('<a href="@user-link">@user</a> @action recurring fee <a href="@fee-link">@fee</a>.', array('@user-link' => url('user/'. $user->uid), '@user' => $user->name, '@action'=> $action,'@fee-link' => url('admin/store/orders/recurring/view/fee/'. $rfid), '@fee' => $rfid)));
  }
}



/**
 * Displays the confirmation form for reactivating a recurring fee.
 */
 
function uc_luottokunta_xml_user_reactivate_form($form_state, $rfid, $fee_handler) {
  $path = uc_referer_uri();
  $fee = uc_recurring_fee_user_load($rfid);

  if ($fee->fee_handler == $fee_handler) {
    $form['uid'] = array('#type' => 'value', '#value' => $fee->uid);
    $form['rfid'] = array('#type' => 'value', '#value' => $rfid);
    
    return confirm_form($form, t('Are you sure you want to reactivate this recurring fee?'), $suspend_path, t('This will reactivate your recurring payments back to normal'), t('Yes, reactivate the subscription'), t('No, keep things as they were'));
  } else {
    drupal_set_message(t('Invalid fee handler was given, try again from the operations for this <a href="@url">recurring fee</a>, if problem persists contact the site administrator.', array('@url' => url($path))), 'error');
  }
}

/**
 * Implements hook_submit() for the cancel recurring fee form
 */
function uc_luottokunta_xml_user_reactivate_form_submit($form, &$form_state) {

  uc_recurring_fee_reactivate($form_state['values']['rfid']);

  $fee = uc_recurring_fee_user_load($form_state['values']['rfid']);
  $form_state['redirect'] = 'user/'. $fee->uid;
}


/**
 * Wrapper function to suspend a user's recurring fee.
 *
 * @param $rfid
 *   The recurring fee's ID.
 * @param $fee
 *   Optional; The loaded fee object.
 */
 
function uc_recurring_fee_reactivate($rfid, $fee = NULL) {
  global $user;
  if (empty($fee)) {
    $fee = uc_recurring_fee_user_load($rfid);
  }
  
  $fee->status = UC_RECURRING_FEE_STATUS_ACTIVE;
  $fee->data['reactivate'] = time();
  $action = 'reactivated';

  if(uc_recurring_fee_user_save($fee)){
    
    drupal_set_message(t('The recurring fee has been @action by your request.', array('@action' => $action)),'status');
    
    // Add comment about cancellation in the product.
    uc_order_comment_save($fee->order_id, $user->uid, t('<a href="@user-link">@user</a> @action recurring fee <a href="@fee-link">@fee</a>.', array('@user-link' => url('user/'. $user->uid), '@user' => $user->name, '@action'=> $action,'@fee-link' => url('admin/store/orders/recurring/view/fee/'. $rfid), '@fee' => $rfid)));
  }
}

